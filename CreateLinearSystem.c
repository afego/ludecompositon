#include <time.h>   //time, rand
#include <stdio.h>  //printf, sprintf
#include <stdlib.h> //malloc, atoi


/*
Compilacao
gcc CreateLinearSystem.c -o CLS
Execucao
./CLS <lynear system order?
*/
void CreateAnswer(char *ansfile, int order){
    //Criando o arquivo para escrita
    FILE *file = fopen(ansfile, "w+");
    //Gerando numeros entre [lowerlimit, upperlimit] e escrevendo-os no arquivo
    int lowerlimit = -100, upperlimit = 100;
    int num;
    for (int i = 0; i < order; i++){
        num = (rand() % (upperlimit - lowerlimit + 1)) + lowerlimit;
        fprintf(file, "%d", num);
        if(i+1<order) fputs("\n",file);
    }
    fclose(file);
}

//Funcao para criar o sistema linear Ax = b
//A matriz A é armazenada no arquivo 'lsfile', a matriz b não é necessária para a Fatoração LU
void CreateLinearSystem(char *lsfile, char *ansfile, char *cfile, int order){
    //Leitura das variáveis para gerar o sistema linear
    FILE *af = fopen(ansfile,"r");
    int *answers = (int*)malloc(order*sizeof(int));
    for(int i=0; i< order; i++){
        fscanf(af, "%d", &answers[i]);
    }
    fclose(af);
    //Gerando constantes entre [lowerlimit, upperlimit] e escrevendo-os no arquivo
    int lowerlimit = -100.0, upperlimit = 100.0;
    //Alocando matriz A de coeficientes
    int **Amatrix = (int**)malloc(order*sizeof(int*));
    for(int i=0; i<order;i++){
        Amatrix[i] = (int*)malloc(order*sizeof(int));
    }
    //Alocando matriz b de constantes com zeros
    int *bmatrix = (int*)calloc(order,sizeof(int));
    //Gerando os multiplicadores do sistema linear em A e calculando a resposta em b
    for(int i=0; i<order; i++){
        for(int j=0; j<order; j++){
            Amatrix[i][j] = ( rand() % (upperlimit - lowerlimit + 1) ) + lowerlimit;
            bmatrix[i] += Amatrix[i][j] * answers[i];
        }
    }
    //Escrevendo a matriz A de coeficientes
    FILE *lsf = fopen(lsfile, "w+");
    for(int i=0; i<order; i++){
        //Escrevendo a matriz por linhas
        for(int j=0; j<order; j++){
            fprintf(lsf, "%d", Amatrix[i][j]);
            //Separo todos os elementos por espaço
            if(j+1<order) fputs(" ",lsf);
        }
        //Pulando para a próxima linha da matriz
        if(i+1<order) fputs("\n",lsf);
    }
    fclose(lsf);
    //Escrevendo a matriz b de constantes
    FILE *cf = fopen(cfile, "w+");
    for(int i=0; i<order; i++){
        fprintf(cf, "%d", bmatrix[i]);
        if(i+1<order) fputs("\n",cf);
    }
    fclose(cf);

}

//CreateLinearSystem.c "ordem da matriz"
int main(int arg, char **argv){
	srand(time(NULL));

    int order = atoi(argv[1]);
    //Nome do arquivo de respostas a ser gerado
    char *answerfile = "answer.txt";
    //Nome do arquivo do sistema linear a ser gerado
    char *linearsystemfile = "linearsystem.txt";
    //Nome do arquivo de constantes
    char *constantfile = "constants.txt";

    CreateAnswer(answerfile, order); //Comente essa linha para gerar somente um novo sistema para as mesmas variáveis
    CreateLinearSystem(linearsystemfile, answerfile, constantfile, order);

}
