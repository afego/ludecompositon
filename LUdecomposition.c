#include <stdio.h>  //printf, fscanf
#include <stdlib.h> //malloc, calloc, atoi

/*
Trabalho de implementação de Laboratório de Programação Paralela e Distribuída
André Fernandes Gonçalves
Daniel Marcondes Bougleux Sodré
José Victor De Paiva e Silva
2020.1
*/

/*
Prototipos das funcoes auxiliares
*/
void printMatrix(float **matrix, int n);
float** initializeMatrix(int n);
void readLinearSystem(char *filename, float **matrix, int n);
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns);
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB);
float** matrixMultiplication(float **mA, float **mB, int order);
float** identityMatrix(int order);

//============================================================================
/*
Versão Sequencial da Fatoração LU
Compilacao
gcc LUdecomposition.c -o LUdecomposition
Execucao
./LUdecomposition <matrix order>
*/
int main(int argc, char **argv){
    int n = atoi(argv[1]);
    char *linearsystemfile = "linearsystem.txt";

    //Leitura da matriz de coeficientes do sistema linear
    //A matriz lida se tornara a Upper
    float **matrix = initializeMatrix(n);
    readLinearSystem(linearsystemfile,matrix,n);
    printf("\nMatriz lida:\n");
    printMatrix(matrix,n);
    //Matriz identidade para armazenar as permutacoes nas linhas de matrix 
    float **rowIdentity = identityMatrix(n);
    //Alocacao da matrizes Lower
    float **lower, pivot, multiplier;;
    lower = initializeMatrix(n);
    int t;
    //Calculo da fatoracao LU
    for(int i=0;i<n;i++){
        lower[i][i] = 1;//a diagonal principal de L eh unitaria
        //(i) analisar os possiveis pivos
        t = i;
        pivot = matrix[t][t];//A primeira tentativa de pivo eh o elemento da diagonal principal
        //(i.1)Analisando possiveis pivos na mesma coluna
        while(pivot == 0 && t<n){//Se o pivo eh nulo, comeco o pivoteamento
            t = t+1;
            if(matrix[t][i] != 0){//Se encontrei um novo pivot, vou permutar as linhas
                pivot = matrix[t][i];
                permuteMatrixRows(matrix, i, t, n);
                permuteMatrixRows(rowIdentity, i, t, n);
            }
        }
        if(pivot != 0){//Aplico a fatoracao somente se encontrar um pivo, se NAO encontrar um novo pivo, passo para a proxima iteracao
            //(ii) Aplicando a fatoracao LU
            for(int j=i+1;j<n;j++){//laco j define a linha da matriz que esta recebendo a multiplicacao
                multiplier = matrix[j][i]/pivot;//Calculo do multiplo para a linha atual
                lower[j][i] = multiplier;
                for(int k=0;k<n;k++){
                    matrix[j][k] = matrix[j][k] - multiplier*matrix[i][k];
                }
            }
        }
    }
    //Impressao dos resultados
    printf("\nMatriz L:\n");
    printMatrix(lower,n);
    printf("\nMatriz U:\n");
    printMatrix(matrix,n);

    //Mostrando que a fatoracao LU com pivoteamento parcial funciona
    // L x U = RI x A x CI

    printf("\nMatriz L x U\n");
    float **luRes = initializeMatrix(n);
    luRes = matrixMultiplication(lower, matrix, n);
    printMatrix(luRes, n);

    printf("\nrowId x Sistema Original\n");
    float **matRes = initializeMatrix(n);
    float **origMatrix = initializeMatrix(n);
    readLinearSystem(linearsystemfile, origMatrix, n);
    matRes = matrixMultiplication(rowIdentity, origMatrix, n);
    printMatrix(matRes, n);
    return 0;
}
//============================================================================
/*
Funcoes auxiliares
*/
void printMatrix(float **matrix, int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%5.2f", matrix[i][j]);
            if(j+1<n) printf(" ");
        }
        printf("\n");
    }
    return;
}

float** initializeMatrix(int n){
    float **matrix = (float**)calloc(n,sizeof(float*));
    for(int i=0;i<n;i++) matrix[i] = (float*)calloc(n,sizeof(float));
    return matrix;
}

void readLinearSystem(char *filename, float **matrix, int n){
    FILE *lsf = fopen(filename, "r");
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            fscanf(lsf, "%f", &matrix[i][j]);
        }
    }
    fclose(lsf);
    return;
}

//Funcao para permutar as linhas rowA e rowB de matrix
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns){
    float *temp = (float*)malloc(columns*sizeof(float));
    temp = matrix[rowA];
    matrix[rowA] = matrix[rowB];
    matrix[rowB] = temp;
}

//Funcao para permutar as colunas colA e colB de matrix
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB){
    float temp;
    for(int i=0;i<rows;i++){
        temp = matrix[i][colA];
        matrix[i][colA] = matrix[i][colB];
        matrix[i][colB] = temp;
    }
}

//Funcao para criacao de uma matriz identidade
float** identityMatrix(int order){
    float **matrix = initializeMatrix(order);
    for(int i=0;i<order;i++){
        matrix[i][i] = 1;
    }
    return matrix;
}

float** matrixMultiplication(float **mA, float **mB, int order){
    float **mC = initializeMatrix(order);
    for(int i=0; i<order; i++){
        for(int j=0; j<order; j++){
            for(int k=0; k<order; k++){
                mC[i][j] += mA[i][k]*mB[k][j];
            }
        }
    }
    return mC;
}