__kernel void LUmult(__global double*Upper, __global double*Lower, int iOrder, __global int*pivot, __global double*mult) {
    int xGID = get_global_id(0); //coluna global do WI
    int yGID = get_global_id(1); //linha global do WI

    double mult_priv;
    int pivot_priv = pivot[0];
    if(Upper[pivot_priv * iOrder + pivot_priv] != 0) { //Se o pivor nao for 0
        if(yGID > pivot_priv && xGID == pivot_priv) { //Se o WI estiver na submatriz de fatoracao do pivo
            //Calcula o multiplicador da sua linha
            mult_priv = Upper[yGID * iOrder + pivot_priv] / Upper[pivot_priv * iOrder + pivot_priv];
            //Se for da coluna do pivot, escreve o multiplicador na matriz lower
            mult[yGID] = mult_priv;
            Lower[yGID * iOrder + xGID] = mult_priv;
        }
    }
    if(xGID == pivot_priv && yGID == pivot_priv){
        Lower[xGID *iOrder + yGID] = 1.0; //Atribui 1 ao elemento da diagonal principal
    }
}