#include <stdio.h>  //printf, fscanf
#include <stdlib.h> //malloc, calloc, atoi
#include <mpi.h>

/*
Trabalho de implementação de Laboratório de Programação Paralela e Distribuída
André Fernandes Gonçalves
Daniel Marcondes Bougleux Sodré
José Victor De Paiva e Silva
2020.1
*/

/*
Prototipos das funcoes auxiliares
*/
void printMatrix(float **matrix, int n);
float** initializeMatrix(int n);
void readLinearSystem(char *filename, float **matrix, int n);
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns);
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB);
float** matrixMultiplication(float **mA, float **mB, int order);
float** identityMatrix(int order);

//============================================================================
/*
Versão Sequencial da Fatoração LU
Compilacao
mpicc LUdecompositionOpenMPI.c -o LUmpi
Execucao
mpiexec -n <n of process> LUmpi <matrix order>
*/
int main(int argc, char **argv){
    int n = atoi(argv[1]);
    char *linearsystemfile = "linearsystem.txt";
    int rank, size, master = 0;
    MPI_Status status;

    //Essas 3 matrizes somente sao acessadas pelo mestre
    float **matrix, **lower, **rowIdentity;
    float pivot, multiplier;
    int *displacement, *quantity, *received, t;
    //Cada processo possuira sua matriz parcial que contem o bloco de linhas
    //o final de cada iteracao, eh necessario enviar as informacoes de cada matriz parcial para a original
    float **partialMatrix;
    //Cada processo precisa ter acesso a linha do pivo da iteracao
    float *pivotLine = (float*)calloc(n, sizeof(float));

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    //Vetor que define qual a linha inicial que cada processo eh responsavel
    displacement = (int*)calloc(size,sizeof(int));
    //Vetor que define a quantidade de linhas em cada bloco de responsabilidade do processo
    quantity = (int*)calloc(size,sizeof(int));
    //Vetor que define quantos multiplos de linha o mestre recebeu de cada processo
    received = (int*)calloc(size,sizeof(int));

    if(rank == master){
        //Leitura da matriz de coeficientes do sistema linear
        //A matriz lida se tornara a Upper
        matrix = initializeMatrix(n);
        readLinearSystem(linearsystemfile,matrix,n);
        //printf("\nMatriz lida:\n");
        //printMatrix(matrix,n);
        //Matriz identidade para armazenar as permutacoes nas linhas de matrix 
        rowIdentity = identityMatrix(n);
        //Alocacao da matriz Lower
        lower = initializeMatrix(n);
    }
    //Calculo da fatoracao LU
    for(int i=0;i<n;i++){
        /*
        Nesta implementacao de memoria distribuida, o mestre nao so eh responsavel pelo pivoteamento
        Mas tambem eh responsavel pela distribuicao do bloco de linhas que cada processo sera responsavel
        Ao final de cada iteracao, eh necessario receber os blocos para atualizar a matriz original, antes de seguir com a proxima iteracao
        */
        //(i) Pivoteamento e distribuicao dos blocos da matriz
        if (rank == master){
            lower[i][i] = 1;//a diagonal principal de L eh unitaria
            t = i;
            pivot = matrix[t][t];//A primeira tentativa de pivo eh o elemento da diagonal principal
            //(i.1)Analisando possiveis pivos na mesma coluna
            while(pivot == 0 && t<n){//Se o pivo eh nulo, comeco o pivoteamento
                t = t+1;
                if(matrix[t][i] != 0){//Se encontrei um novo pivot, vou permutar as linhas
                    pivot = matrix[t][i];
                    permuteMatrixRows(matrix, i, t, n);
                    permuteMatrixRows(rowIdentity, i, t, n);
                }
            }
            //(i.2)Analise e distribuicao das linhas restantes para os processos
            int quotient = (n-i-1) / size;//(n-i) determina a quantidade de linhas que nao foram completamente fatoradas
            int remainder = (n-i-1) % size;//-1 impede o envio da linha do pivo para os processos
            //todos os processos comecarao com a mesma quantidade de linhas restantes da matriz
            for(int k=0;k<size;k++){
                quantity[k] = quotient;
            }
            //distribuindo o resto igualmente entre os processos
            int z = 0;
            while(remainder > 0){
                quantity[z] +=1;
                remainder -=1;
                z++;
                if(z == size) z = 0;
            }
            //analisando a linha inicial de seu bloco na matriz para cada processo
            for(int k=0; k<size;k++){
                if(k==0) displacement[k] = i+1;//O bloco do processo mestre sempre comeca na linha da iteracao
                else displacement[k] = displacement[k-1] + quantity[k-1];
            }
            //Enviando as informacoes do tamanho da matriz parcial de cada processo
            for(int k=1;k<size;k++){
                MPI_Send(quantity, size, MPI_INT, k, 0, MPI_COMM_WORLD);
                MPI_Send(displacement, size, MPI_INT, k, 1, MPI_COMM_WORLD);
            }
            //(i.3) Enviando as informacoes do pivo
            //Copiando a linha do pivo do proprio mestre
            for(int k=0;k<n;k++){
                pivotLine[k] = matrix[i][k];
            }
            //Enviando para os trabalhadores
            for(int k=1;k<size;k++){
                if(quantity[k] > 0){
                    MPI_Send(&pivot, 1, MPI_FLOAT, k, 2, MPI_COMM_WORLD);
                    MPI_Send(pivotLine, n, MPI_FLOAT, k, 2, MPI_COMM_WORLD);
                }
            }
            //(i.4) Enviando o bloco de linhas que compoe cada matriz parcial
            for(int k=1;k<size;k++){
                if(quantity[k] > 0){//Ignoro os processos que nao receberao linhas nessa iteracao
                    for(int x = displacement[k]; x < displacement[k] + quantity[k]; x++){
                        MPI_Send(matrix[x], n, MPI_FLOAT, k, 3, MPI_COMM_WORLD);
                    }
                }
            }
            //Alocando a matriz parcial do proprio mestre
            if(quantity[master] > 0){
                partialMatrix = (float**)calloc(quantity[master], sizeof(float*));
                for(int k=0; k < quantity[master]; k++){
                    partialMatrix[k] = (float*)calloc(n,sizeof(float));
                }
                z = 0;
                for(int x=displacement[master]; x < displacement[master] + quantity[master]; x++){
                    for(int y=0;y<n;y++){
                        partialMatrix[z][y] = matrix[x][y];
                    }
                    z++;
                }
            }            
        //(i)Recebimento do pivoteamento e dos blocos de linhas da matriz
        }else{
            //(i.2) Recebendo as informacoes da matriz parcial
            MPI_Recv(quantity, size, MPI_INT, master, 0, MPI_COMM_WORLD, &status);
            MPI_Recv(displacement, size, MPI_INT, master, 1, MPI_COMM_WORLD, &status);

            if(quantity[rank] > 0){
                //(i.3) Recebendo as informacoes do pivo
                MPI_Recv(&pivot, 1, MPI_FLOAT, master, 2, MPI_COMM_WORLD, &status);
                MPI_Recv(pivotLine, n, MPI_FLOAT, master, 2, MPI_COMM_WORLD, &status);
                
                //Alocacao da matriz parcial de 'quantity' linhas e 'n' colunas
                partialMatrix = (float**)calloc(quantity[rank], sizeof(float*));
                for(int k=0;k<quantity[rank];k++) partialMatrix[k] = (float*)calloc(n,sizeof(float));
                
                //(i.4)Recebendo o bloco de linhas de cada processo        
                for(int x=0; x<quantity[rank];x++){
                    MPI_Recv(partialMatrix[x], n, MPI_FLOAT, master, 3, MPI_COMM_WORLD, &status);
                }
            }  
        }

        //(ii) Aplicacao da fatoracao LU
        if(pivot != 0 && quantity[rank] > 0){//Aplico a fatoracao somente se encontrar um pivo, se NAO encontrar um novo pivo, passo para a proxima iteracao
            //(ii.1)Fatoracao LU nas matrizes parciais de cada processo
            for(int j=0;j<quantity[rank];j++){//laco j define a linha da matriz que esta recebendo a multiplicacao       
                multiplier = partialMatrix[j][i]/pivot;//Calculo do multiplo para a linha atual
                //Atualizacao da matriz L
                if(rank == master){
                    //Laco para atualizar L com os multiplicadores dos trabalhadores
                    for(int k=1;k<size;k++){
                        //Mesmo que quantity[k] > 0, o mestre pode estar em um laco j maior que o maximo do rank k
                        //Por isso a utilizacao de received[k], para ter certeza que o mestre ja recebeu todos os multiplos que tinha para receber do processo k
                        if(quantity[k] > 0 && (received[k] < quantity[k])){
                            //MPI_Recv(&t, 1, MPI_INT, k, 4, MPI_COMM_WORLD, &status); //Esse Send/Recv muito provavelmente nao eh necessario
                            //Convertendo o indice da matriz parcial do trabalhador para a matriz real
                            //t = t + displacement[k];
                            t = j + displacement[k];
                            //Recebendo o multiplicador
                            MPI_Recv(&lower[t][i], 1, MPI_FLOAT, k, 4, MPI_COMM_WORLD, &status);
                            received[k]++;
                        }
                    }
                    //atualizando L com o multiplicador do proprio mestre
                    t = j + displacement[master];
                    lower[t][i] = multiplier;
                }else{
                    //Enviando o indice da linha na matriz parcial do trabalhador
                    //MPI_Send(&j, 1, MPI_INT, master, 4, MPI_COMM_WORLD);
                    //Enviando o multiplicador
                    MPI_Send(&multiplier, 1, MPI_FLOAT, master, 4, MPI_COMM_WORLD);
                }
                //Fatoracao LU no bloco de cada processo
                for(int k=0;k<n;k++){
                    partialMatrix[j][k] = partialMatrix[j][k] - multiplier*pivotLine[k];
                }
            }
            //(ii.2) Recebimento dos resultados da iteracao
            if(rank == master){
                //Atualizacao da matriz original com a fatoracao dos trabalhadores
                for(int k=1;k<size;k++){
                    if(quantity[k] > 0){
                        for(int x = displacement[k]; x < displacement[k] + quantity[k]; x++){
                            MPI_Recv(matrix[x], n, MPI_FLOAT, k, 5, MPI_COMM_WORLD, &status);  
                        }
                    }
                    //O laco acabou, preciso zerar o received para o proximo laco
                    received[k] = 0;
                }
                //Atualizacao da matriz original com o proprio mestre
                int z = 0;
                for(int x = displacement[master]; x < displacement[master] + quantity[master]; x++){
                    for(int y = 0; y < n; y++){
                        matrix[x][y] = partialMatrix[z][y];
                    }
                    z++;
                }
            }else{
                //Enviando a matriz parcial dos trabalhadores
                for(int x = 0; x < quantity[rank]; x++){
                    MPI_Send(partialMatrix[x], n, MPI_FLOAT, master, 5, MPI_COMM_WORLD);
                }
            }
        }
    }
    if(rank == master){
        //Impressao dos resultados
        printf("\nMatriz L:\n");
        printMatrix(lower,n);
        printf("\nMatriz U:\n");
        printMatrix(matrix,n);

        //Mostrando que a fatoracao LU com pivoteamento parcial funciona
        // L x U = RI x A x CI

        printf("\nMatriz L x U\n");
        float **luRes = initializeMatrix(n);
        luRes = matrixMultiplication(lower, matrix, n);
        printMatrix(luRes, n);

        printf("\nrowId x Sistema Original\n");
        float **matRes = initializeMatrix(n);
        float **origMatrix = initializeMatrix(n);
        readLinearSystem(linearsystemfile, origMatrix, n);
        matRes = matrixMultiplication(rowIdentity, origMatrix, n);
        printMatrix(matRes, n);
    }
    
    MPI_Finalize();

    return 0;
}
//============================================================================
/*
Funcoes auxiliares
*/
void printMatrix(float **matrix, int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%5.2f", matrix[i][j]);
            if(j+1<n) printf(" ");
        }
        printf("\n");
    }
    return;
}

float** initializeMatrix(int n){
    float **matrix = (float**)calloc(n,sizeof(float*));
    for(int i=0;i<n;i++) matrix[i] = (float*)calloc(n,sizeof(float));
    return matrix;
}

void readLinearSystem(char *filename, float **matrix, int n){
    FILE *lsf = fopen(filename, "r");
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            fscanf(lsf, "%f", &matrix[i][j]);
        }
    }
    fclose(lsf);
    return;
}

//Funcao para permutar as linhas rowA e rowB de matrix
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns){
    float *temp = (float*)malloc(columns*sizeof(float));
    temp = matrix[rowA];
    matrix[rowA] = matrix[rowB];
    matrix[rowB] = temp;
}

//Funcao para permutar as colunas colA e colB de matrix
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB){
    float temp;
    for(int i=0;i<rows;i++){
        temp = matrix[i][colA];
        matrix[i][colA] = matrix[i][colB];
        matrix[i][colB] = temp;
    }
}

//Funcao para criacao de uma matriz identidade
float** identityMatrix(int order){
    float **matrix = initializeMatrix(order);
    for(int i=0;i<order;i++){
        matrix[i][i] = 1;
    }
    return matrix;
}

float** matrixMultiplication(float **mA, float **mB, int order){
    float **mC = initializeMatrix(order);
    for(int i=0; i<order; i++){
        for(int j=0; j<order; j++){
            for(int k=0; k<order; k++){
                mC[i][j] += mA[i][k]*mB[k][j];
            }
        }
    }
    return mC;
}