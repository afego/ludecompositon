#include <stdio.h>  //printf, fscanf
#include <stdlib.h> //malloc, calloc, atoi
#include <omp.h>

/*
Trabalho de implementação de Laboratório de Programação Paralela e Distribuída
André Fernandes Gonçalves
Daniel Marcondes Bougleux Sodré
José Victor De Paiva e Silva
2020.1
*/

/*
Prototipos das funcoes auxiliares
*/
void printMatrix(float **matrix, int n);
float** initializeMatrix(int n);
void readLinearSystem(char *filename, float **matrix, int n);
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns);
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB);
float** matrixMultiplication(float **mA, float **mB, int order);
float** identityMatrix(int order);

//============================================================================
/*
Versão Paralela da Fatoração LU usando OpenMP
Compilacao
gcc LUdecompositionOpenMP.c -o LUomp -fopenmp
Execucao
./LUomp <matrix order> <n threads>
*/
int main(int argc, char **argv){
    int n = atoi(argv[1]);
    int numthreads = atoi(argv[2]), rank;
    omp_set_num_threads(numthreads);
    //Nome do arquivo que contem a matriz de coeficientes do sistema linear
    char *linearsystemfile = "linearsystem.txt";

    //Leitura da matriz de coeficientes do sistema linear
    //A matriz lida se tornara a Upper
    float **matrix = initializeMatrix(n);
    readLinearSystem(linearsystemfile,matrix,n);
    printf("\nMatriz lida:\n");
    printMatrix(matrix,n);
    //Matriz identidade para armazenar as permutacoes nas linhas de matrix 
    float **rowIdentity = identityMatrix(n);

    //Alocacao da matriz Lower
    float **lower, pivot, multiplier;
    lower = initializeMatrix(n);
    int t;//variavel auxiliar para o pivoteamento
    int x;
    //Calculo da fatoracao LU
    for(int i=0;i<n;i++){
        #pragma omp parallel private(multiplier, rank)
        {
            rank = omp_get_thread_num();
            //Nesta implementacao somente uma thread eh responsavel pelo pivoteamento
            #pragma omp single 
            //essa diretiva possui uma barreira implicita em seu final
            //assim, nenhuma thread executara a fatoracao LU sem antes haver um mesmo pivo para todos 
            {
                lower[i][i] = 1;//a diagonal principal de L eh unitaria
                //(i) analisar os possiveis pivos
                t = i;
                pivot = matrix[t][t];//A primeira tentativa de pivo eh o elemento da diagonal principal
                //(i.1)Analisando possiveis pivos na mesma coluna
                while(pivot == 0 && t<n){//Se o pivo eh nulo, comeco o pivoteamento
                    t = t+1;
                    if(matrix[t][i] != 0){//Se encontrei um novo pivo, vou permutar as linhas
                        pivot = matrix[t][i];
                        permuteMatrixRows(matrix, i, t, n);
                        permuteMatrixRows(rowIdentity, i, t, n);
                    }
                }
            }
            if(pivot != 0){//Aplico a fatoracao somente se encontrar um pivo, se NAO encontrar um novo pivo, passo para a proxima iteracao
                //(ii) Aplicando a fatoracao LU
                for(int j=i+1 + rank; j < n; j = j + numthreads){//laco j define a linha da matriz que esta recebendo a multiplicacao
                    multiplier = matrix[j][i]/pivot;//Calculo do multiplo para a linha atual
                    lower[j][i] = multiplier;
                    for(int k=0;k<n;k++){
                        //#pragma omp critical
                        //{
                        //    matrix[j][k] = matrix[j][k] - multiplier*matrix[i][k];
                        //}
                        matrix[j][k] = matrix[j][k] - multiplier*matrix[i][k];
                    }
                }
            } 
        }
    }
    
    //Impressao dos resultados
    printf("\nMatriz L:\n");
    printMatrix(lower,n);
    printf("\nMatriz U:\n");
    printMatrix(matrix,n);

    //Mostrando que a fatoracao LU com pivoteamento parcial funciona
    // L x U = RI x A

    printf("\nMatriz L x U\n");
    float **luRes = initializeMatrix(n);
    luRes = matrixMultiplication(lower, matrix, n);
    printMatrix(luRes, n);

    printf("\nrowId x Sistema Original\n");
    float **matRes = initializeMatrix(n);
    float **origMatrix = initializeMatrix(n);
    readLinearSystem(linearsystemfile, origMatrix, n);
    matRes = matrixMultiplication(rowIdentity, origMatrix, n);
    printMatrix(matRes, n);

    return 0;
}
//============================================================================
/*
Funcoes auxiliares
*/
void printMatrix(float **matrix, int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%5.2f", matrix[i][j]);
            if(j+1<n) printf(" ");
        }
        printf("\n");
    }
    return;
}

float** initializeMatrix(int n){
    float **matrix = (float**)calloc(n,sizeof(float*));
    for(int i=0;i<n;i++) matrix[i] = (float*)calloc(n,sizeof(float));
    return matrix;
}

void readLinearSystem(char *filename, float **matrix, int n){
    FILE *lsf = fopen(filename, "r");
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            fscanf(lsf, "%f", &matrix[i][j]);
        }
    }
    fclose(lsf);
    return;
}

//Funcao para permutar as linhas rowA e rowB de matrix
void permuteMatrixRows(float **matrix, int rowA, int rowB, int columns){
    float *temp = (float*)malloc(columns*sizeof(float));
    temp = matrix[rowA];
    matrix[rowA] = matrix[rowB];
    matrix[rowB] = temp;
}

//Funcao para permutar as colunas colA e colB de matrix
void permuteMatrixColumns(float **matrix, int rows, int colA, int colB){
    float temp;
    for(int i=0;i<rows;i++){
        temp = matrix[i][colA];
        matrix[i][colA] = matrix[i][colB];
        matrix[i][colB] = temp;
    }
}

//Funcao para criacao de uma matriz identidade
float** identityMatrix(int order){
    float **matrix = initializeMatrix(order);
    for(int i=0;i<order;i++){
        matrix[i][i] = 1;
    }
    return matrix;
}

float** matrixMultiplication(float **mA, float **mB, int order){
    float **mC = initializeMatrix(order);
    for(int i=0; i<order; i++){
        for(int j=0; j<order; j++){
            for(int k=0; k<order; k++){
                mC[i][j] += mA[i][k]*mB[k][j];
            }
        }
    }
    return mC;
}