__kernel void LUdecomp(__global double*Upper, __global double*mults, int iOrder, __global int*pivot, __global int*j) {
    int xGID = get_global_id(0); //coluna global do WI
    int yGID = get_global_id(1); //linha global do WI

    int pivot_priv = pivot[0];
    if(Upper[pivot_priv * iOrder + pivot_priv] != 0) { //Se o pivor nao for 0
        if(yGID > pivot_priv && xGID >= pivot_priv) { //Se o WI estiver na submatriz de fatoracao do pivo
            if(xGID == 29 && yGID == 30 && pivot_priv == 28) {
                printf("A fatoracao eh:\n");
                printf("Upper[30][29] = %.3f - %.3f X %.3f\n", Upper[yGID * iOrder + xGID], mults[yGID], Upper[pivot_priv * iOrder + xGID]);
            }
            //fatoracao propriamente dita
            Upper[yGID * iOrder + xGID] = Upper[yGID * iOrder + xGID] - mults[yGID]*Upper[pivot_priv * iOrder + xGID];
        }
    }
    if(!xGID && !yGID) {
        printf("\nO Pivot [%d] eh: %.3f\n", pivot_priv, Upper[pivot_priv * iOrder + pivot_priv]);
        for(int i=0; i<iOrder; i++) {
            for(int j=0; j<iOrder; j++)printf("%.0f ", Upper[i * iOrder + j]);
            printf("\n");
        }
    }
    if(!xGID && !yGID) j[0]++;
}