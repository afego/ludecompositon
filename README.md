Trabalho de Laboratório de Programação Paralela e Distribuída

André Fernandes Gonçalves
Daniel Marcondes Bougleux Sodré
José Victor de Paiva e Silva

É necessário haver um arquivo de sistema linear para todas as versões de Fatoração LU "LUdecomposition*.c".

i) CreateLinearSystem.c
Programa necessário para gerar o sistema linear a receber a Fatoração LU.
Ele gera dois arquivos:
-'answer.txt':
	Arquivo onde o valor das incógnitas do sistema linear a ser gerado.
-'linearsystem.txt':
	Arquivo onde o sistema linear é de fato armazenado.
Esse programa primeiramente cria as variáveis e em seguida cria um sistema linear baseado nelas, ambos aleatoriamente.
O arquivo de sistema linear é necessário e usado nos arquivos de Fatoração LU.

ii) LUdecomposition.c
Programa que aplica a Fatoração LU com pivoteamento parcial de forma sequencial.

iii)LUdecompositionOpenMP.c
Programa de Fatoração LU com pivoteamento parcial de forma paralela. Possui uma abordagem Mestre-Trabalhador, onde o mestre é responsável pelo pivoteamento.
A Fatoração LU em si é feita em paralelo.

iv)LUdecompositionOpenMPI.c
Programa de Fatoração LU com pivoteamento parcial de forma distribuída. Assim como a versão em OpenMP, o mestre é responsável pelo pivoteamento.

v)LUdecompositionOpenCL.c
Programa de Fatoração LU com pivoteamento parcial de forma paralela. É necessário o arquivo LUdecomp.cl para sua execução.