#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
/*
Trabalho de implementação de Laboratório de Programação Paralela e Distribuída
André Fernandes Gonçalves
Daniel Marcondes Bougleux Sodré
José Victor De Paiva e Silva
2020.1
*/
/*
Prototipos das funcoes auxiliares para Matriz
*/
void printMatrix(float *matrix, int n);
cl_double* initializeMatrix(int n);
void readLinearSystem(char *filename, cl_double *matrix, int n);
void permuteMatrixRows(float *matrix, int rowA, int rowB, int columns);
float* matrixMultiplication(float *mA, float *mB, int order);
float* identityMatrix(int order);
void verifyMatrix(char*arquivoNomeA, char*arquivoNomeB, int n);
void writeToFile(cl_double*matriz, int n, char*arquivoNome);
//============================================================================
/*
Prototipos das funcoes auxiliares para OpenCL GPU
*/
void getDevices(cl_int* statusPtr, cl_platform_id* platformPtr, cl_uint* numDevicesPtr, cl_device_id*devicePtr);
void getPlatform(cl_int* ciErrPtr, cl_platform_id* platformPtr);
void getContext(cl_int* statusPtr, cl_context*contextPtr, cl_uint* numDevicesPtr, cl_device_id* devicePtr);
void getCmdQueue(cl_int* statusPtr, cl_command_queue* cmdQueuePtr, cl_context* contextPtr, cl_device_id* devicePtr);
void getProgram(cl_int* ciErrPtr, FILE**programHandlePtr, char*arquivoNome, size_t* programSizePtr, char**programBufferPtr,
                cl_program* cpProgramPtr, cl_context* contextPtr);
void buildProgram(cl_int* ciErrPtr, cl_program* cpProgramPtr, cl_device_id* devicePtr);
void getKernel(cl_int* ciErrPtr, cl_kernel* ckKernelPtr, cl_program* cpProgramPtr, char*kernelName);
//============================================================================
/*
Versão OpenCL da Fatoração LU
Compilacao
gcc -I/usr/local/cuda/include LUdecompositionOpenCL.c -o exec_cl -lOpenCL
Execucao
./exec_cl <matrix order>
*/
int main(int argc, char **argv){
    cl_int status; //Status OpenCL de operacoes
    cl_int ciErr;  //Para avisar de houve erro em alguma operacao OpenCL
    cl_platform_id platform;  //Plataforma de drivers e dispositivos
    cl_command_queue cmdQueue;  //Fila de comandos OpenCL para dispositivos em contexto específico
    cl_device_id device;  //Dispositivo na plataforma
    cl_context context = NULL;  //Contexto OpenCL de dispositivos numa plataforma
    cl_uint numDevices = 0;  // Quantos dispositivos achamos
    cl_int iOrder;  //Ordem da matriz
    FILE* programHandle;  //stream de arquivo para o source code
    size_t programSize;  //Tamanho do source code do kernel
    char*programBuffer;  //Buffer com o nome do source code do kernel
    cl_program cpProgram; //Programa do kernel em estrutura OpenCL
    cl_kernel ckKernel;  //Kernel em estrutura OpenCL
    cl_event kernelevent; //Estrutura para eventos no Kernel

    FILE* programHandle2;  //stream de arquivo para o source code
    size_t programSize2;  //Tamanho do source code do kernel
    char*programBuffer2;  //Buffer com o nome do source code do kernel
    cl_program cpProgram2; //Programa do kernel em estrutura OpenCL
    cl_kernel ckKernel2;  //Kernel em estrutura OpenCL
    cl_event kernelevent2; //Estrutura para eventos no Kernel

    FILE* programHandle3;  //stream de arquivo para o source code
    size_t programSize3;  //Tamanho do source code do kernel
    char*programBuffer3;  //Buffer com o nome do source code do kernel
    cl_program cpProgram3; //Programa do kernel em estrutura OpenCL
    cl_kernel ckKernel3;  //Kernel em estrutura OpenCL
    cl_event kernelevent3; //Estrutura para eventos no Kernel

    int n = atoi(argv[1]); //Recebe a ordem da matriz por linha de comando
    iOrder = n;  //Ordem de Inteiro OpenCL é a Ordem de Inteiro

    cl_int iWI; //Dimensao dos work groups
    cl_int szWG; //Dimensao global das GPU threads
    if(iOrder <= 32){ //Para receber matriz de ordem < 16
        iWI = iOrder;
        szWG = iWI;
    }
    else { //Fixa em 16 o tamanho dos work groups
        iWI = 32;
        szWG = iOrder;
    }
    const size_t szLocalWorkSize[2] = { iWI , iWI }; //Work Groups serao 16 X 16
    const size_t szGlobalWorkSize[2] = { szWG, szWG }; //Total de Work Itens sera iOrder X iOrder

    //Leitura da matriz de coeficientes do sistema linear no arquivo
    //A matriz lida se tornara a Upper
    cl_double *matrix = initializeMatrix(n); 
    char *linearsystemfile = "linearsystem.txt";
    readLinearSystem(linearsystemfile,matrix,n);
    //Alocacao da matriz Lower
    cl_double *lower;
    lower = initializeMatrix(n);
    //Reconhece uma plataforma no sistema e cria uma estrutura baseada nela
    getPlatform(&ciErr, &platform);
    //Reconhece dispositivos na plataforma e cria as estruturas baseada nela
    getDevices(&status, &platform, &numDevices, &device);
    //Cria um contexto OpenCL para o dispositivo GPU na plataforma
    getContext(&status, &context, &numDevices, &device);
    //Cria uma fila de comandos OpenCL para o contexto e seu dispositivo
    getCmdQueue(&status, &cmdQueue, &context, &device);
    printf("Context and command queue set...\n");
    //Recebe o programa com o source code do Kernel
    getProgram(&ciErr, &programHandle, "LUdecomp.cl", &programSize, &programBuffer, &cpProgram, &context);
     //Recebe o programa com o source code do Kernel
    getProgram(&ciErr, &programHandle2, "LUpivot.cl", &programSize2, &programBuffer2, &cpProgram2, &context);
    //Recebe o programa com o source code do Kernel
    getProgram(&ciErr, &programHandle3, "LUmult.cl", &programSize3, &programBuffer3, &cpProgram3, &context);
    //Compila o programa com o kernel
    buildProgram(&ciErr, &cpProgram, &device);
    //Compila o programa com o kernel
    buildProgram(&ciErr, &cpProgram2, &device);
    //Compila o programa com o kernel
    buildProgram(&ciErr, &cpProgram3, &device);
    printf("Kernel program built and set...\n");
    cl_int i_kernel[1] = {0};
    cl_mem bufferI;
    bufferI = clCreateBuffer(context,0,sizeof(cl_int),NULL,&status);
    cl_int j_kernel[1] = {0};
    cl_mem bufferJ;
    bufferJ = clCreateBuffer(context,0,sizeof(cl_int),NULL,&status);
    cl_double*mults = (void*)calloc(n,sizeof(cl_double));
    cl_mem bufferM;
    bufferM = clCreateBuffer(context,0,n*sizeof(cl_double),NULL,&status);
    cl_mem bufferL; //Buffer de memória OpenCL, para tratar de enderecamento diferentes
    cl_mem bufferU; //para dispositivos diferentes. Memoria em GPU e diferente de CPU
    size_t datasize = sizeof(cl_double) * iOrder*iOrder; //Tamanho dos Buffers que receberao a matriz
    //Cria buffer no contexto da GPU, apenas para escrita, do tamanho iOrder X iOrder
    bufferL = clCreateBuffer(context,CL_MEM_WRITE_ONLY,datasize,NULL,&status);
    //Cria buffer no contexto da GPU, leitura e escrita, do tamanho iOrder X iOrder
    bufferU = clCreateBuffer(context,0,datasize,NULL,&status);
    //Cria o Kernel baseado no programa com o kernel source code
    getKernel(&ciErr, &ckKernel, &cpProgram, "LUdecomp");
    //Escreve os valores de Lower no bufferL
    status = clEnqueueWriteBuffer(cmdQueue,bufferL,CL_FALSE,0,datasize,lower,0,NULL,NULL);
    //Escreve os valores de Upper no bufferU
    status = clEnqueueWriteBuffer(cmdQueue,bufferU,CL_FALSE,0,datasize,matrix,0,NULL,NULL);
    status = clEnqueueWriteBuffer(cmdQueue,bufferI,CL_FALSE,0,sizeof(cl_int),i_kernel,0,NULL,NULL);
    status = clEnqueueWriteBuffer(cmdQueue,bufferJ,CL_FALSE,0,sizeof(cl_int),j_kernel,0,NULL,NULL);
    status = clEnqueueWriteBuffer(cmdQueue,bufferM,CL_FALSE,0,n*sizeof(cl_double),mults,0,NULL,NULL);
    //Prepara os argumentos do kernel da fatoração
    ciErr = clSetKernelArg(ckKernel,0,sizeof(cl_mem),(void*)&bufferU);//Primeiro argumento
    ciErr |= clSetKernelArg(ckKernel,1,sizeof(cl_mem),(void*)&bufferM);//Segundo argumento
    ciErr |= clSetKernelArg(ckKernel,2,sizeof(cl_int),(void*)&iOrder);//Terceiro argumento
    ciErr |= clSetKernelArg(ckKernel,3,sizeof(cl_mem),(void*)&bufferI);//Quarto argumento
    ciErr |= clSetKernelArg(ckKernel,4,sizeof(cl_mem),(void*)&bufferJ);//Quinto argumento
    if(ciErr != CL_SUCCESS) {
        printf("Error setting kernel args!\n");
    }
    //Cria o Kernel baseado no programa com o kernel source code
    getKernel(&ciErr, &ckKernel2, &cpProgram2, "LUpivot");
    //Prepara os argumentos do kernel do pivoteamente
    ciErr = clSetKernelArg(ckKernel2,0,sizeof(cl_mem),(void*)&bufferU);//Primeiro argumento
    ciErr |= clSetKernelArg(ckKernel2,1,sizeof(cl_int),(void*)&iOrder);//Segundo argumento
    ciErr |= clSetKernelArg(ckKernel2,2,sizeof(cl_mem),(void*)&bufferI);//Terceiro argumento
    ciErr |= clSetKernelArg(ckKernel2,3,sizeof(cl_mem),(void*)&bufferJ);//Quarto argumento
    if(ciErr != CL_SUCCESS) {
        printf("Error setting kernel args!\n");
    }
    //Cria o Kernel baseado no programa com o kernel source code
    getKernel(&ciErr, &ckKernel3, &cpProgram3, "LUmult");
    //Prepara os argumentos do kernel do pivoteamente
    ciErr = clSetKernelArg(ckKernel3,0,sizeof(cl_mem),(void*)&bufferU);//Primeiro argumento
    ciErr |= clSetKernelArg(ckKernel3,1,sizeof(cl_mem),(void*)&bufferL);//Segundo argumento
    ciErr |= clSetKernelArg(ckKernel3,2,sizeof(cl_int),(void*)&iOrder);//Terceiro argumento
    ciErr |= clSetKernelArg(ckKernel3,3,sizeof(cl_mem),(void*)&bufferI);//Quarto argumento
    ciErr |= clSetKernelArg(ckKernel3,4,sizeof(cl_mem),(void*)&bufferM);//Quinto argumento
    if(ciErr != CL_SUCCESS) {
        printf("Error setting kernel args!\n");
    }
    printf("Buffers created and asynchronously written...\n");
    //Alguma coisa
    for(int i=0;i<n;i++){
        //Enfilera o N-Dimension Range de Work Itens e executa o Kernel. A grade de Work Itens tera 2 dimensoes
        ciErr = clEnqueueNDRangeKernel(cmdQueue,ckKernel2,2,NULL,szGlobalWorkSize,szLocalWorkSize,0,NULL,&kernelevent2);
        if(ciErr != CL_SUCCESS) {
            printf("Errr number: %d\n", ciErr);
            printf("Error lauching kernel!\n");
        }
        //Espera o kernel terminar de executar
        clFinish(cmdQueue);

        //Enfilera o N-Dimension Range de Work Itens e executa o Kernel. A grade de Work Itens tera 2 dimensoes
        ciErr = clEnqueueNDRangeKernel(cmdQueue,ckKernel3,2,NULL,szGlobalWorkSize,szLocalWorkSize,0,NULL,&kernelevent3);
        if(ciErr != CL_SUCCESS) {
            printf("Errr number: %d\n", ciErr);
            printf("Error lauching kernel!\n");
        }
        //Espera o kernel terminar de executar
        clFinish(cmdQueue);

        //Enfilera o N-Dimension Range de Work Itens e executa o Kernel. A grade de Work Itens tera 2 dimensoes
        ciErr = clEnqueueNDRangeKernel(cmdQueue,ckKernel,2,NULL,szGlobalWorkSize,szLocalWorkSize,0,NULL,&kernelevent);
        if(ciErr != CL_SUCCESS) {
            printf("Errr number: %d\n", ciErr);
            printf("Error lauching kernel!\n");
        }
        //Espera o kernel terminar de executar
        clFinish(cmdQueue);
    }

    printf("Returned from kernel...\n");
    //Le os valores dos buffers da GPU nas matrizes da RAM
    ciErr = clEnqueueReadBuffer(cmdQueue,bufferU,CL_TRUE,0,datasize,matrix,0,NULL,NULL);
    ciErr = clEnqueueReadBuffer(cmdQueue,bufferL,CL_TRUE,0,datasize,lower,0,NULL,NULL);
    //Espera terminar de ler os valores dos buffers
    clFinish(cmdQueue);
    printf("Returned values read...\n");
    //Escreve as matrizes L e U em arquivos
    writeToFile(lower, n, "Lower.txt");
    writeToFile(matrix, n, "Upper.txt");
    //Compara com as respostas da versao sequencial
    verifyMatrix("Upper.txt", "UpperSeq.txt", n);
    verifyMatrix("Lower.txt", "LowerSeq.txt", n);

    if(ckKernel) clReleaseKernel(ckKernel); //Libera estrutura de kernel OpenCL
    if(cpProgram) clReleaseProgram(cpProgram); //Libera estrutura do programa kernel OpenCL
    if(cmdQueue) clReleaseCommandQueue(cmdQueue); //Libera estrutura da fila de comandos OpenCL
    if(context) clReleaseContext(context); //Libera estrutura do contexto OpenCL
    if(bufferU) clReleaseMemObject(bufferU); //Libera Buffers de GPU OpenCL
    if(bufferL) clReleaseMemObject(bufferL); //Libera Buffers de GPU OpenCL

    return 0;
}
//=================================================================================================
//=================================================================================================
//=================================================================================================
//=================================================================================================
//=================================================================================================
void printMatrix(float *matrix, int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%5.2f", matrix[i * n + j]);
            if(j+1<n) printf(" ");
        }
        printf("\n");
    }
    return;
}

cl_double* initializeMatrix(int n){
    cl_double *matrix = (void*)calloc(n*n,sizeof(cl_double));
    return matrix;
}

void readLinearSystem(char *filename, cl_double *matrix, int n){
    FILE *lsf = fopen(filename, "r");
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            fscanf(lsf, "%lf", &matrix[i * n + j]);
        }
    }
    fclose(lsf);
    return;
}

//Funcao para permutar as linhas rowA e rowB de matrix
void permuteMatrixRows(float *matrix, int rowA, int rowB, int columns){
    float *temp = (void*)malloc(columns*sizeof(cl_float));
    for(int i=0; i<columns; i++)temp[i] = matrix[rowA * columns + i];
    for(int i=0; i<columns; i++)matrix[rowA * columns + i] = matrix[rowB * columns + i];
    for(int i=0; i<columns; i++)matrix[rowB * columns + i] = temp[i];
}

//Funcao para criacao de uma matriz identidade
float* identityMatrix(int order){
    float *matrix = initializeMatrix(order);
    for(int i=0;i<order;i++){
        matrix[i * order + i] = 1;
    }
    return matrix;
}

float* matrixMultiplication(float *mA, float *mB, int order){
    float *mC = initializeMatrix(order);
    for(int i=0; i<order; i++){
        for(int j=0; j<order; j++){
            for(int k=0; k<order; k++){
                mC[i * order + j] += mA[i * order + k]*mB[k * order + j];
            }
        }
    }
    return mC;
}

void verifyMatrix(char*arquivoNomeA, char*arquivoNomeB, int n) {
    FILE*arquivoA = fopen(arquivoNomeA, "r");
    FILE*arquivoB = fopen(arquivoNomeB, "r");
    printf("\nComparing %s...\n", arquivoNomeA);
    int dif = 0;
    cl_double value1, value2;
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++) {
            fscanf(arquivoA, "%lf", &value1);
            fscanf(arquivoB, "%lf", &value2);
            if(value1 - value2 > 4 || value1 - value2 < -4) {
                dif = 1;
                printf("Difference is %.3lf in line %d collumn %d : %.3lf != %.3lf\n", value1 - value2, i, j, value1, value2);
            }
        }
    }
    fclose(arquivoA);
    fclose(arquivoB);
    if(!dif)printf("No differentes were found!\n");
    else printf("There was at least one difference between the two files!\n");
}

void writeToFile(cl_double*matriz, int n, char*arquivoNome) {
    FILE*arquivo = fopen(arquivoNome, "w");
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            //if(!matriz[i * n + j]) memset(&matriz[i * n + j], 0, sizeof(cl_double));
            fprintf(arquivo, "%.3lf ", matriz[i * n + j]);
        }
        fprintf(arquivo, "\n");
    }
    fclose(arquivo);
}
//================================================================================================
//================================================================================================
//================================================================================================
//================================================================================================
//================================================================================================
void getPlatform(cl_int* ciErrPtr, cl_platform_id* platformPtr) {
    (*ciErrPtr) = clGetPlatformIDs(1, platformPtr, NULL);
    if((*ciErrPtr) != CL_SUCCESS){
        printf("Error: Failed to create platform!\n");
        exit(1);
    } 
    char platformName[128];
    clGetPlatformInfo((*platformPtr), CL_PLATFORM_NAME, 128, platformName, NULL);
    printf("Platform name = %s\n", platformName);
}

void getDevices(cl_int* statusPtr, cl_platform_id* platformPtr, cl_uint* numDevicesPtr, cl_device_id*devicePtr) {
    (*statusPtr) = clGetDeviceIDs(
        (*platformPtr),
        CL_DEVICE_TYPE_GPU,
        0,
        NULL,
        numDevicesPtr
    );
    if((*statusPtr) != CL_SUCCESS) {
        printf("Error: Failed to find number of devices!\n");
        exit(EXIT_FAILURE);
    }
    printf("The number of GPU devices found = %d\n", (*numDevicesPtr));
    (*statusPtr) = clGetDeviceIDs(
        (*platformPtr),
        CL_DEVICE_TYPE_GPU,
        (*numDevicesPtr),
        devicePtr,
        NULL
    );
    if((*statusPtr) != CL_SUCCESS) {
        printf("Error: Failed to create a device group!\n");
        exit(EXIT_FAILURE);
    }
}

void getContext(cl_int* statusPtr, cl_context*contextPtr, cl_uint* numDevicesPtr, cl_device_id* devicePtr) {
    (*contextPtr) = clCreateContext(
        NULL,
        (*numDevicesPtr),
        devicePtr,
        NULL,
        NULL,
        statusPtr
    );
    if(!(*contextPtr)) {
        printf("Error: Failed to create a compute context!\n");
        exit(EXIT_FAILURE);
    }
}

void getCmdQueue(cl_int* statusPtr, cl_command_queue* cmdQueuePtr, cl_context* contextPtr, cl_device_id* devicePtr) {
    (*cmdQueuePtr) = clCreateCommandQueue(
        (*contextPtr),
        (*devicePtr),
        CL_QUEUE_PROFILING_ENABLE,
        statusPtr
    );
    if(!(*cmdQueuePtr)) {
        printf("Error: Failed to create a command queue!\n");
        exit(EXIT_FAILURE);
    }
}

void getProgram(cl_int* ciErrPtr, FILE**programHandlePtr, char*arquivoNome, size_t* programSizePtr, char**programBufferPtr,
                cl_program* cpProgramPtr, cl_context* contextPtr) {
    (*programHandlePtr) = fopen(arquivoNome, "r");
    fseek((*programHandlePtr), 0 , SEEK_END);
    (*programSizePtr) = ftell((*programHandlePtr));
    rewind((*programHandlePtr));

    (*programBufferPtr) = (char*) malloc((*programSizePtr) + 1);
    (*programBufferPtr)[(*programSizePtr)] = '\0';
    fread((*programBufferPtr), sizeof(char), (*programSizePtr), (*programHandlePtr));
    fclose((*programHandlePtr));

    (*cpProgramPtr) = clCreateProgramWithSource(
        (*contextPtr),
        1,
        (const char **)programBufferPtr,
        programSizePtr,
        ciErrPtr
    );
    if(!(*cpProgramPtr)){
        printf("Error: Failed to create compute program!\n");
        exit(EXIT_FAILURE);
    }
    free((*programBufferPtr));
}

void buildProgram(cl_int* ciErrPtr, cl_program* cpProgramPtr, cl_device_id* devicePtr) {
    (*ciErrPtr) = clBuildProgram(
        (*cpProgramPtr),
        0,
        NULL,
        NULL,
        NULL,
        NULL
    );
    if( (*ciErrPtr) != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(
            (*cpProgramPtr),
            (*devicePtr),
            CL_PROGRAM_BUILD_LOG,
            sizeof(buffer),
            buffer,
            &len
        );
        printf("%s\n", buffer);
        exit(1);
    }
}

void getKernel(cl_int* ciErrPtr, cl_kernel* ckKernelPtr, cl_program* cpProgramPtr, char*kernelName) {
    (*ckKernelPtr) = clCreateKernel((*cpProgramPtr),kernelName,ciErrPtr);
    if(!(*ckKernelPtr) || (*ciErrPtr) != CL_SUCCESS) {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
}