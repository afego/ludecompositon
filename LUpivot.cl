__kernel void LUpivot(__global double*Upper, int iOrder, __global int*i, __global int*j) {
    int xGID = get_global_id(0); //coluna global do WI
    int yGID = get_global_id(1); //linha global do WI

    double temp;
    double pivot;
    int t;
    int i_priv;

    if(!xGID && !yGID){//Se for a GPU thread master
        i[0] = j[0];
        i_priv = i[0];
        t = i_priv;
        pivot = Upper[t * iOrder + t]; //Tentamos o proprio elemento da diagonal principal como pivo
        while(pivot == 0 && t < iOrder) { //Enquanto houver elemento nulo ali
            t++; //Busca a proxima linha
            if(Upper[t * iOrder + i_priv] != 0) { //Se encontrou elemento nao nulo
                pivot = Upper[t * iOrder + i_priv]; //Ele vira o pivo
                for(int j=0; j<iOrder; j++) { //Faz a troca de linhas
                    temp = Upper[i_priv * iOrder + j];
                    Upper[i_priv * iOrder + j] = Upper[t * iOrder + j];
                    Upper[t * iOrder + j] = temp;
                }
            }
        }
    }
}