__kernel void matrixMul(__global float*matrixA, __global float*matrixB, __global float*matrixC, int N){
    #define TILE_WIDTH 16
    //local memory to fit the tiles
    __local float matrixAsub[TILE_WIDTH * TILE_WIDTH];
    __local float matrixBsub[TILE_WIDTH * TILE_WIDTH];

    //global thread index
    int xGID = get_global_id(0); //column in NDRange
    int yGID = get_global_id(1); //row in NDRange

    // local thread index
    int xLID = get_local_id(0);// column in tile
    int yLID = get_local_id(1);// row in tile

    float dotprod = 0.0;

    for (int tile = 0; tile < N / TILE_WIDTH; tile ++) {
        //Collaborative loading of tiles into shared memory:
        //Load a tile of matrix A into local memory
        matrixAsub[yLID * N + xLID] = matrixA [yGID * N + (xLID + tile * TILE_WIDTH)];
        //Load a tile of matrix B into local memory
        matrixBsub[yLID * N + xLID] = matrixB[(yLID + tile * TILE_WIDTH) * N + xGID];
        //Synchronise to make sure the tiles are loaded
        barrier(CLK_LOCAL_MEM_FENCE);
        for (int i = 0; i < TILE_WIDTH; i ++) {
            dotprod += matrixAsub[yLID * N + i] * matrixBsub[i * N + xLID];
        }
        //Wait for other work-items to finish
        //beforeloading next tile
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    matrixC[yGID * N + xGID] = dotprod;
}